package com.fi.smartscan.network;

import com.fi.smartscan.model.Scan;

import java.util.List;

import io.reactivex.Single;
import retrofit2.http.GET;

/**
 * Created by anand on 2019-05-05 at 13:56.
 */
public interface ScanApi {

    @GET("data")
    public Single<List<Scan>> getScans();
}
