package com.fi.smartscan.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.fi.smartscan.R;
import com.fi.smartscan.feature.scandetail.ScanDetailActivity;
import com.fi.smartscan.model.Color;
import com.fi.smartscan.model.Scan;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ananddubey
 * Dated- 2019-05-06
 */
public class ScanAdapter extends RecyclerView.Adapter<ScanAdapter.ScanViewHolder> {

    private List<Scan> scans = new ArrayList<>();

    public ScanAdapter(List<Scan> scans) {
        this.scans.addAll(scans);
    }

    @NonNull
    @Override
    public ScanViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        return new ScanViewHolder(inflater.inflate(R.layout.item_list_scan,
                parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull final ScanViewHolder scanViewHolder, int position) {

        final Scan scan = scans.get(position);
        scanViewHolder.updateScan(scan);

        scanViewHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                scanViewHolder.context.startActivity(ScanDetailActivity.getIntent(scan, scanViewHolder.context));
            }
        });
    }

    public void swapAll(List<Scan> scans) {
        Log.d("ADAPTER ", "swapAll: " + scans);
        if (scans != null) {
            if (!this.scans.isEmpty())
                this.scans.clear();
            this.scans.addAll(scans);
            notifyDataSetChanged();
        }
    }

    @Override
    public int getItemCount() {
        return scans.size();
    }

    static class ScanViewHolder extends RecyclerView.ViewHolder {

        AppCompatTextView mScanName;
        AppCompatTextView mScanTag;

        Context context;

        ScanViewHolder(@NonNull View itemView) {
            super(itemView);
            context = itemView.getContext();
            mScanName = itemView.findViewById(R.id.scan_name);
            mScanTag = itemView.findViewById(R.id.scan_tag);
        }

        void updateScan(Scan scan) {
            if (scan != null) {
                if (!scan.name.isEmpty())
                    mScanName.setText(scan.name);
                if (!scan.tag.isEmpty())
                    mScanTag.setText(scan.tag);
                if (scan.color != null) {
                    if (scan.color.equals(Color.GREEN))
                        mScanTag.setTextColor(context.getResources().getColor(R.color.green_tag));
                    else if (scan.color.equals(Color.RED))
                        mScanTag.setTextColor(context.getResources().getColor(R.color.red_tag));
                }
            }
        }
    }
}
