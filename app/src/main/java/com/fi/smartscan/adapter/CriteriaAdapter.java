package com.fi.smartscan.adapter;

import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.RecyclerView;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.fi.smartscan.R;
import com.fi.smartscan.model.Criteria;
import com.fi.smartscan.model.Variable;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by ananddubey
 * Dated- 2019-05-07
 */

public class CriteriaAdapter extends RecyclerView.Adapter<CriteriaAdapter.CriteriaViewHolder> {

    private static final String TAG = "CriteriaAdapter";
    private List<Criteria> criterias = new ArrayList<>();
    private OnVariableClickListener variableClickListener;

    public CriteriaAdapter(List<Criteria> criterias, OnVariableClickListener variableClickListener) {
        if (criterias != null && !criterias.isEmpty())
            this.criterias.addAll(criterias);
        this.variableClickListener = variableClickListener;
    }


    @NonNull
    @Override
    public CriteriaViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        return new CriteriaViewHolder(inflater.inflate(R.layout.item_list_criteria,
                parent, false), variableClickListener);
    }

    @Override
    public void onBindViewHolder(@NonNull CriteriaViewHolder criteriaViewHolder, int position) {
        criteriaViewHolder.updateCriteria(criterias.get(position));
    }

    public void swapAll(List<Criteria> criterias) {
        if (criterias != null && !criterias.isEmpty()) {
            if (!this.criterias.isEmpty())
                this.criterias.clear();
            this.criterias.addAll(criterias);
            notifyDataSetChanged();
        }
    }

    @Override
    public int getItemCount() {
        return criterias.size();
    }

    static class CriteriaViewHolder extends RecyclerView.ViewHolder {
        AppCompatTextView mCriteriaInfo;
        OnVariableClickListener variableListener;

        Map<Integer, String> startValues = new HashMap<>();

        void updateCriteria(Criteria criteria) {
            startValues.clear();
            if (criteria != null) {
                if (criteria.isPlainText())
                    mCriteriaInfo.setText(criteria.text);
                else if (criteria.isVariable()) {
                    String displayString = getDisplayText(criteria.text, criteria.variables);
                    mCriteriaInfo.setText(getSpanText(displayString, criteria.variables));
                    mCriteriaInfo.setMovementMethod(LinkMovementMethod.getInstance());
                }
            }
            Log.e(TAG, "updateCriteria: " + startValues);
        }

        CriteriaViewHolder(@NonNull View itemView, OnVariableClickListener variableListener) {
            super(itemView);
            this.variableListener = variableListener;
            mCriteriaInfo = itemView.findViewById(R.id.criteria_info);
        }

        String getDisplayText(String displayText, Map<String, Variable> variableMap) {
            for (Map.Entry<String, Variable> entry : variableMap.entrySet()) {
                Variable currentVariable = entry.getValue();
                String currentKey = entry.getKey();
                startValues.put(displayText.indexOf(currentKey), currentKey);
                if (currentVariable.isValue())
                    displayText = displayText.replace(currentKey, String.valueOf(currentVariable.values.get(0)));
                else if (currentVariable.isIndicator())
                    displayText = displayText.replace(currentKey, String.valueOf(currentVariable.defaultValue));
            }
            return displayText;
        }

        Spannable getSpanText(String displayText, final Map<String, Variable> variableMap) {
            SpannableString displaySpan = new SpannableString(displayText);
            if (variableMap != null && !variableMap.isEmpty()) {
                for (final Map.Entry<Integer, String> entry : startValues.entrySet()) {
                    int currentKey = entry.getKey();
                    String currentValue = entry.getValue();
                    int endIndex = 0;

                    if (variableMap.get(currentValue).isValue())
                        endIndex = currentKey + String.valueOf(variableMap.get(currentValue).values.get(0)).length();
                    else
                        endIndex = currentKey + String.valueOf(variableMap.get(currentValue).defaultValue).length();

                    displaySpan.setSpan(new ClickableSpan() {
                        @Override
                        public void onClick(@androidx.annotation.NonNull @NonNull View widget) {
                            if (variableListener != null)
                                variableListener.onVariableSelected(entry.getValue(), variableMap.get(entry.getValue()));
                        }
                    }, entry.getKey(), endIndex, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                    displaySpan.setSpan(new ForegroundColorSpan(Color.BLUE), entry.getKey(), endIndex, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                }
            }
            return displaySpan;
        }
    }
}
