package com.fi.smartscan.adapter;

import com.fi.smartscan.model.Variable;

/**
 * Created by ananddubey
 * Dated- 2019-05-08
 */
public interface OnVariableClickListener {
    void onVariableSelected(String variableSelected, Variable variable);
}
