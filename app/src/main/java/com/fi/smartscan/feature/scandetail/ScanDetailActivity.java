package com.fi.smartscan.feature.scandetail;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;

import com.fi.smartscan.R;
import com.fi.smartscan.model.Color;
import com.fi.smartscan.model.Scan;
import com.fi.smartscan.model.Variable;

/**
 * Created by ananddubey
 * Dated- 2019-05-07
 */
public class ScanDetailActivity extends AppCompatActivity {

    RecyclerView mCriteriaRecyclerView;
    AppCompatTextView mScanName;
    AppCompatTextView mScanTag;

    ScanDetailViewModel viewModel;
    private static final String TAG = "ScanDetailActivity";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_scan_detail);

        mScanName = findViewById(R.id.scan_name);
        mScanTag = findViewById(R.id.scan_tag);
        mCriteriaRecyclerView = findViewById(R.id.criteria_list);

        Scan scan = (Scan) getIntent().getSerializableExtra(ScanDetailViewModel.KEY_SCAN_DETAILS);

        mCriteriaRecyclerView = findViewById(R.id.criteria_list);
        mCriteriaRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        mCriteriaRecyclerView.setHasFixedSize(true);
        mCriteriaRecyclerView.setAdapter(getViewModel().getAdapter());

        getViewModel().getVariableClicked().observe(this, new Observer<Variable>() {
            @Override
            public void onChanged(@Nullable Variable variable) {
                if (variable != null)
                    startActivity(VariableListActivity.getIntent(ScanDetailActivity.this, variable));
            }
        });
        if (scan != null) {
            mScanName.setText(scan.name);
            mScanTag.setText(scan.tag);
            if (scan.color.equals(Color.GREEN))
                mScanTag.setTextColor(getResources().getColor(R.color.green_tag));
            else
                mScanTag.setTextColor(getResources().getColor(R.color.red_tag));
            Log.d("SCAN DETAIL", "onCreate: " + scan);
            getViewModel().getAdapter().swapAll(scan.criterias);
        }
    }

    public static Intent getIntent(Scan scan, Context context) {
        Intent intent = new Intent(context, ScanDetailActivity.class);
        intent.putExtra(ScanDetailViewModel.KEY_SCAN_DETAILS, scan);
        return intent;
    }

    public ScanDetailViewModel getViewModel() {
        if (viewModel == null)
            viewModel = ViewModelProviders.of(this).get(ScanDetailViewModel.class);
        return viewModel;
    }
}
