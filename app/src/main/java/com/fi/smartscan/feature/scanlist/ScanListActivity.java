package com.fi.smartscan.feature.scanlist;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;

import com.fi.smartscan.R;

public class ScanListActivity extends AppCompatActivity {

    RecyclerView mScanRecyclerView;
    ScanListViewModel viewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_scan_list);

        mScanRecyclerView = findViewById(R.id.scan_list);
        mScanRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        mScanRecyclerView.setHasFixedSize(true);
        mScanRecyclerView.setAdapter(getViewModel().getAdapter());

        getViewModel().getScanFetched()
                .observe(this, new Observer<Boolean>() {
                    @Override
                    public void onChanged(@Nullable Boolean fetched) {
                        Log.d("SCAN ACT VM", "onCHANGED: " + fetched);
                        if (fetched != null && fetched)
                            mScanRecyclerView.setVisibility(View.VISIBLE);
                        else mScanRecyclerView.setVisibility(View.GONE);

                    }
                });
        getViewModel().fetchScans();
    }

    public ScanListViewModel getViewModel() {
        if (viewModel == null)
            viewModel = ViewModelProviders.of(this).get(ScanListViewModel.class);
        return viewModel;
    }
}
