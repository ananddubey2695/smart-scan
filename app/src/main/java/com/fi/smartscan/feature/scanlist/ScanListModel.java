package com.fi.smartscan.feature.scanlist;

import com.fi.smartscan.model.Scan;
import com.fi.smartscan.network.NetworkManager;

import java.util.List;

import io.reactivex.Single;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by ananddubey
 * Dated- 2019-05-06
 */
class ScanListModel {
    ScanListModel() {
    }

    Single<List<Scan>> getScans() {
        return NetworkManager.getInstance()
                .getScanApi()
                .getScans()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }
}
