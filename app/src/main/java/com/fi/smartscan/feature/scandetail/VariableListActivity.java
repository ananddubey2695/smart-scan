package com.fi.smartscan.feature.scandetail;

import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatEditText;
import android.support.v7.widget.AppCompatTextView;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.fi.smartscan.R;
import com.fi.smartscan.model.Variable;

import java.util.Collections;

/**
 * Created by ananddubey
 * Dated- 2019-05-08
 */
public class VariableListActivity extends AppCompatActivity {

    private static final String TAG = "VariableListActivity";
    ScanDetailViewModel viewModel;

    ConstraintLayout mIndicatorLayout;
    AppCompatTextView mVariableName;
    AppCompatTextView mParameterName;
    AppCompatEditText mParameterInput;
    ListView mValueList;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_variable_details);
        mIndicatorLayout = findViewById(R.id.indicator_layout);
        mVariableName = findViewById(R.id.variable_name);
        mParameterName = findViewById(R.id.parameter_type);
        mParameterInput = findViewById(R.id.parameter_input);
        mValueList = findViewById(R.id.value_list);

        Variable variable = (Variable) getIntent().getSerializableExtra(ScanDetailViewModel.KEY_VARIABLE_LIST);

        if (variable != null) {
            if (variable.isIndicator()) {
                if (!variable.studyType.isEmpty())
                    mVariableName.setText(variable.studyType);
                if (!variable.parameterName.isEmpty())
                    mParameterName.setText(variable.parameterName);

                mIndicatorLayout.setVisibility(View.VISIBLE);
                mValueList.setVisibility(View.GONE);
            } else if (variable.isValue() && !variable.values.isEmpty()) {
                Collections.sort(variable.values);
                ArrayAdapter<Double> valueAdapter =
                        new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, variable.values);
                mValueList.setAdapter(valueAdapter);

                mValueList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
                        Log.e(TAG, "onItemClick: " + position);
                    }
                });
                mIndicatorLayout.setVisibility(View.GONE);
                mValueList.setVisibility(View.VISIBLE);
            } else {
                mIndicatorLayout.setVisibility(View.GONE);
                mValueList.setVisibility(View.GONE);
            }
        }
    }

    public static Intent getIntent(Context context, Variable variables) {
        Intent intent = new Intent(context, VariableListActivity.class);
        intent.putExtra(ScanDetailViewModel.KEY_VARIABLE_LIST, variables);
        return intent;
    }

    public ScanDetailViewModel getViewModel() {
        if (viewModel == null)
            viewModel = ViewModelProviders.of(this).get(ScanDetailViewModel.class);
        return viewModel;
    }
}
