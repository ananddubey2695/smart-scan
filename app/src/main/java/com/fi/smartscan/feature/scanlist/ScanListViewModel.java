package com.fi.smartscan.feature.scanlist;

import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;
import android.util.Log;

import com.fi.smartscan.adapter.ScanAdapter;
import com.fi.smartscan.model.Scan;

import java.util.Collections;
import java.util.List;

import io.reactivex.SingleObserver;
import io.reactivex.disposables.Disposable;

/**
 * Created by ananddubey
 * Dated- 2019-05-06
 */
public class ScanListViewModel extends ViewModel {

    private ScanListModel model;
    private ScanAdapter adapter = new ScanAdapter(Collections.<Scan>emptyList());
    private MutableLiveData<Boolean> scanFetched = new MutableLiveData<>();

    public ScanListViewModel() {
        model = new ScanListModel();
    }

    ScanAdapter getAdapter() {
        return adapter;
    }

    void fetchScans() {
        model.getScans()
                .subscribe(new SingleObserver<List<Scan>>() {
                    @Override
                    public void onSubscribe(Disposable d) {

                    }

                    @Override
                    public void onSuccess(List<Scan> scans) {
                        Log.d("SCAN VM", "onSuccess: " + scans);
                        if (scans != null && !scans.isEmpty())
                            adapter.swapAll(scans);
                        scanFetched.postValue(true);
                    }

                    @Override
                    public void onError(Throwable e) {
                        scanFetched.postValue(false);
                    }
                });
    }

    public MutableLiveData<Boolean> getScanFetched() {
        return scanFetched;
    }
}
