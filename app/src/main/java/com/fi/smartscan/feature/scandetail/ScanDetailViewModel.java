package com.fi.smartscan.feature.scandetail;

import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;

import com.fi.smartscan.adapter.CriteriaAdapter;
import com.fi.smartscan.adapter.OnVariableClickListener;
import com.fi.smartscan.model.Criteria;
import com.fi.smartscan.model.Variable;

import java.util.Collections;

/**
 * Created by ananddubey
 * Dated- 2019-05-07
 */
public class ScanDetailViewModel extends ViewModel {
    static final String KEY_SCAN_DETAILS = "SCAN_DETAILS";

    static final String KEY_VARIABLE_LIST = "VARIABLE_LIST";

    private String selectedVariable = "";

    private MutableLiveData<Variable> variableClicked = new MutableLiveData<>();

    public ScanDetailViewModel() {
    }

    private CriteriaAdapter adapter = new CriteriaAdapter(Collections.<Criteria>emptyList(),
            new OnVariableClickListener() {
                @Override
                public void onVariableSelected(String variableSelected, Variable variable) {
                    selectedVariable = variableSelected;
                    variableClicked.postValue(variable);
                }
            });

    CriteriaAdapter getAdapter() {
        return adapter;
    }

    MutableLiveData<Variable> getVariableClicked() {
        return variableClicked;
    }
}
