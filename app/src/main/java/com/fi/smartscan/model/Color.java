package com.fi.smartscan.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by ananddubey
 * Dated- 2019-05-06
 */
public enum Color {
    @SerializedName("green")
    GREEN,
    @SerializedName("red")
    RED
}
