package com.fi.smartscan.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by anand on 2019-05-05 at 13:38.
 */
enum VariableType {
    @SerializedName("value")
    VALUE,

    @SerializedName("indicator")
    INDICATOR
}
