package com.fi.smartscan.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by anand on 2019-05-05 at 13:36.
 */
public class Variable extends Core {

    public VariableType type;

    public List<Double> values;

    @SerializedName("study_type")
    public String studyType;

    @SerializedName("parameter_name")
    public String parameterName;

    @SerializedName("min_value")
    public Integer minValue;

    @SerializedName("max_value")
    public Integer maxValue;

    @SerializedName("default_value")
    public Integer defaultValue;

    public Variable(VariableType type, List<Double> values) {
        this.type = type;
        this.values = values;
    }

    public Variable(VariableType type, String studyType, String parameterName, Integer minValue, Integer maxValue, Integer defaultValue) {
        this.type = type;
        this.studyType = studyType;
        this.parameterName = parameterName;
        this.minValue = minValue;
        this.maxValue = maxValue;
        this.defaultValue = defaultValue;
    }

    public boolean isValue() {
        return type.equals(VariableType.VALUE);
    }

    public boolean isIndicator() {
        return type.equals(VariableType.INDICATOR);
    }

    public Variable() {
    }
}
