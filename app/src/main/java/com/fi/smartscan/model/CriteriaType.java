package com.fi.smartscan.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by anand on 2019-05-05 at 13:13.
 */
public enum CriteriaType {
    @SerializedName("variable")
    VARIABLE,

    @SerializedName("plain_text")
    PLAINTEXT
}
