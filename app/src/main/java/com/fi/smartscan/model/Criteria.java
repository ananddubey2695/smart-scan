package com.fi.smartscan.model;

import com.google.gson.annotations.SerializedName;

import java.util.Map;

/**
 * Created by anand on 2019-05-05 at 12:56.
 */
public class Criteria extends Core {

    public CriteriaType type;

    public String text;

    @SerializedName("variable")
    public Map<String, Variable> variables;

    public Criteria() {
    }

    public Criteria(CriteriaType type, String text, Map<String, Variable> variables) {
        this.type = type;
        this.text = text;
        this.variables = variables;
    }

    public Criteria(CriteriaType type, String text) {
        this.type = type;
        this.text = text;
    }

    public boolean isVariable() {
        return this.type.equals(CriteriaType.VARIABLE);
    }

    public boolean isPlainText() {
        return this.type.equals(CriteriaType.PLAINTEXT);
    }
}
