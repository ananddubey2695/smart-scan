package com.fi.smartscan.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by anand on 2019-05-05 at 12:54.
 */
public class Scan extends Core {
    public String id;
    public String name;
    public String tag;
    public Color color;

    @SerializedName("criteria")
    public List<Criteria> criterias;

    public Scan(String id, String name, String tag, Color color, List<Criteria> criterias) {
        this.id = id;
        this.name = name;
        this.tag = tag;
        this.color = color;
        this.criterias = criterias;
    }
}
